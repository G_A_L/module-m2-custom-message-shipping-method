define([
    'uiComponent',
    'Magento_Checkout/js/model/quote'
],
    function (
    Component,
    quote
    ) {

    'use strict';
    var show_hide_custom_blockConfig = window.checkoutConfig.show_hide_custom_block;
    var price_limit = window.checkoutConfig.price_limit;
    var message;
        if (quote.totals().subtotal >= price_limit) {
            message = window.checkoutConfig.message_high;
        } else {
            message = window.checkoutConfig.message_low;
        }

    return Component.extend({
        defaults: {
            template: 'SR_AdditionalShippingBlock/checkout/shipping/additional-block'
        },
        canVisibleBlock: show_hide_custom_blockConfig,
        message: message
    });
});
