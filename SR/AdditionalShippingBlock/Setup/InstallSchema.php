<?php

namespace SR\AdditionalShippingBlock\Setup;

use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\PageRepository;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @property BlockFactory blockFactory
 * @property BlockRepository blockRepository
 * @property PageRepository pageRepository
 */

class InstallSchema implements InstallSchemaInterface
{
    /**
     * InstallSchema constructor.
     *
     * @param BlockFactory $blockFactory
     * @param PageRepository $pageRepository
     * @param BlockRepository $blockRepository
     */
    public function __construct(
        BlockFactory $blockFactory,
        PageRepository $pageRepository,
        BlockRepository $blockRepository
    ){
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->pageRepository = $pageRepository;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $blocks = array();
        $blocks[] = array(
            'title'      => 'Message Shipping Low',
            'identifier' => 'message_low',
            'content'    => 'Message Shipping Low',
        );
        $blocks[] = array(
            'title'      => 'Message Shipping High',
            'identifier' => 'message_high',
            'content'    => 'Message Shipping High',
        );

        foreach ($blocks as $block) {

            $seStaticBlock = [
                'title'      => $block['title'],
                'identifier' => $block['identifier'],
                'stores'     => [0],
                'is_active'  => 1,
                'content'    => $block['content'],
            ];

            $newLeftBlock = $this->blockFactory->create(['data' => $seStaticBlock]);
            $this->blockRepository->save($newLeftBlock);
        }

        $setup->endSetup();
    }
}