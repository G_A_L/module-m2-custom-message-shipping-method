<?php
namespace SR\AdditionalShippingBlock\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Store\Model\ScopeInterface;

class CustomBlockConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfiguration;


    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfiguration
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfiguration
    ) {
        $this->scopeConfiguration = $scopeConfiguration;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $showHide = [];
        $enabled = $this->scopeConfiguration->getValue('sr_block_config/general/enabled', ScopeInterface::SCOPE_STORE);
        $priceLimit = $this->scopeConfiguration->getValue('sr_block_config/general/price_limit', ScopeInterface::SCOPE_STORE);
        $showHide['show_hide_custom_block'] = ($enabled)?true:false;
        $showHide['price_limit'] = ($priceLimit)?$priceLimit:'79';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $block = $objectManager->create('Magento\Cms\Block\Block');
        $showHide['message_low'] = $block->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId('message_low')->toHtml();
        $showHide['message_high'] = $block->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId('message_high')->toHtml();


        return $showHide;
    }
}